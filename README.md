# MY PERSONAL FORK OF ST
------------------------
st is a simple terminal emulator for X which sucks less. This build of st does not have the scrollback patch because, it's intended to be used along side of tmux.


## Requirements
---------------
In order to build st you need the Xlib header files.
For emoji support, make sure to compile (or download and install) libxft with the bgra patch.


# Installation
--------------
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install


## Running st
-------------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

## Credits
----------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.
